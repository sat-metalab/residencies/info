# Informations

Informations générales au sujet des résidences.

General informations about residencies.

## Public events

### Bureau ouvert - office hours 

URL: [bureau ouvert - SAT](https://sat.qc.ca/fr/nouvelles/bureau-ouvert-virtuel-de-la-sat) --- [bureau ouvert - Metalab](https://sat-metalab.gitlab.io/fr/#bureau-ouvert)

Chaque vendredi, de 13h à 14h, un ou des membres du Metalab ou de la SAT accueille les gens du public pour discuter de façon informelle des projets en cours, des logiciels, etc.

Each Friday, from 1pm to 2pm, at least one member of the SAT or the Metalab host an informal discussion about our projects, our tools, etc.

### 5@7 SAT

URL : [5@7 SAT - SAT](https://sat.qc.ca/fr/evenements/5-sat-soirees-reseautage-arts-industries)

Chaque premier mardi du mois au Café SAT : soirée de réseautage arts et industries.

Every first Tuesday of the month at Café SAT:  arts & industry networking sessions.

## Internal events

### Présentations du vendredi - Friday talks

URL: [présentations passées](https://gitlab.com/sat-metalab/workshops/metalab_presentations)

Chaque vendredi à 11h. Présentations par et pour le Metalab sur des sujets qui nous sont importants. En ligne.

Every Friday at 11am. Talks by and for the Metalab on topics that matters to us. Online.


